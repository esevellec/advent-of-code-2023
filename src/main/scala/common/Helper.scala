package common

import scala.util.{Failure, Success, Try}

object Helper:
  def parseFile[T](module: String, filename: String)(using
      reader: String => Try[T]
  ): Try[T] =
    val file        = os.pwd / "assets" / module / filename
    val fileContent = os.read(file)
    reader(fileContent)

  def reverseTryList[T](listOfTry: List[Try[T]]): Try[List[T]] =
    Try(
      listOfTry.map {
        case Failure(exception) => throw exception
        case Success(value)     => value
      }
    )
