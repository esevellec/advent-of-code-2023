package day8

import day8.PathFinder.startingNode

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

case class PathFinder(
    instructions: List[Char],
    nodes: Map[String, (String, String)],
    startPattern: Option[String] = Some(startingNode),
    previousNodes: List[String] = List.empty,
    currentStep: Int = 0
):
  val currentNodes: List[String] = startPattern
    .map(pattern => nodes.keys.filter(_.endsWith(pattern)).toList)
    .getOrElse(previousNodes)

  def moveToEnd: Try[PathFinder] =
    PathFinder.getEndOfPath(this)

  private def getNextMove(
      getDirection: ((String, String)) => String
  ): PathFinder =
    val destNodes = for
      currentNode <- currentNodes
      destNode <- nodes
        .get(currentNode)
      destNodeValue = getDirection(destNode)
    yield destNodeValue
    PathFinder(
      instructions.tail :+ instructions.head,
      nodes,
      None,
      destNodes,
      currentStep + 1
    )

object PathFinder:
  val startingNode = "A"
  val endingNode   = "Z"

  given (String => Try[PathFinder]) = fromFile
  def fromFile(fileContent: String): Try[PathFinder] =
    Try {
      val instructionsAndNodes = fileContent.split("\n\n")
      val maybeInstructions: Option[List[Char]] =
        instructionsAndNodes.headOption.map(_.toCharArray).map(_.toList)

      val nodesRegex = """^(\w+) = \((\w+), (\w+)\)$""".r
      val maybeNodes = instructionsAndNodes.tail.headOption
        .map(
          _.split("\n").toList.map(line =>
            val nodesRegex(source, left, right) = line
            (source, (left, right))
          )
        )
        .map(_.toMap)
      (for
        instructions <- maybeInstructions
        nodes        <- maybeNodes
      yield PathFinder(instructions, nodes)).get
    }

  @tailrec
  private def getEndOfPath(pathFinder: PathFinder): Try[PathFinder] =
    if pathFinder.currentNodes.forall(_.endsWith(endingNode)) then
      println(s"${pathFinder.currentStep} : ${pathFinder.currentNodes}")
      Success(pathFinder)
    else
      val direction = pathFinder.instructions.head
      (if direction == 'L' then
         println(s"${pathFinder.currentStep} : ${pathFinder.currentNodes}")
         Success(pathFinder.getNextMove(t => t._1))
       else if direction == 'R' then
         println(s"${pathFinder.currentStep} : ${pathFinder.currentNodes}")
         Success(pathFinder.getNextMove(t => t._2))
       else
         Failure[PathFinder](
           new Exception(s"Cannot parse direction $direction")
         )
      )
      match
        case f: Failure[PathFinder] => f
        case Success(pf)            => getEndOfPath(pf)

end PathFinder
