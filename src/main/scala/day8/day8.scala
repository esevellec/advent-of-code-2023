package day8

import common.Helper

import scala.util.{Failure, Success, Try}

@main
def main(): Unit =
  val maybePF: Try[PathFinder] = Helper.parseFile("day8", "input.txt")
  maybePF match
    case Failure(exception) =>
      println(s"Error when parsing puzzle input : ${exception.getMessage}")
    case Success(pathFinder) =>
      println(
        s"There are ${pathFinder.moveToEnd.map(_.currentStep)} step required to reach ZZZ"
      )
