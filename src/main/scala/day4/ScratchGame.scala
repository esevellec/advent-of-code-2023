package day4

import common.Helper
import day4.ScratchGame.scratchACard

import scala.annotation.tailrec
import scala.util.Try

case class ScratchGame(cards: List[ScratchCard]):
  def getGamePoints: Int = cards.map(_.getPoints).sum
  def getTotalCardsAfterScratch: Int =
    scratchACard(0, cards).map(_.nbCards).sum
end ScratchGame

object ScratchGame:

  given (String => Try[ScratchGame]) = createFromFile

  @tailrec
  private def scratchACard(
      cardId: Int,
      currentCards: List[ScratchCard]
  ): List[ScratchCard] =
    if cardId == currentCards.length then currentCards
    else
      val currentCard = currentCards(cardId)
      val matchingNumbers =
        currentCard.nbMatchingNumbers
      val idsToBump = (1 to matchingNumbers).map(_ + cardId).toList
      val currentCardsWithDoubloons =
        idsToBump.foldLeft(currentCards)((cards, cardIdToBump) =>
          addDoubloonsForId(cardIdToBump, cards, currentCard.nbCards)
        )
      scratchACard(cardId + 1, currentCardsWithDoubloons)

  private def addDoubloonsForId(
      cardId: Int,
      currentCards: List[ScratchCard],
      doubloonsToAdd: Int
  ): List[ScratchCard] =
    if cardId >= currentCards.length then currentCards
    else
      val card = currentCards(cardId)
      currentCards.updated(
        cardId,
        card.copy(doubloons = card.doubloons + doubloonsToAdd)
      )

  def createFromFile(fileContent: String): Try[ScratchGame] =
    Helper
      .reverseTryList(
        fileContent
          .split("\n")
          .toList
          .map(line => ScratchCard.createFromLine(line))
      )
      .map(ScratchGame.apply)

end ScratchGame
