package day4

import common.Helper

import scala.util.{Failure, Success, Try}

@main
def main(): Unit =
  val maybeScratchGame: Try[ScratchGame] =
    Helper.parseFile("day4", "input.txt")

  maybeScratchGame match
    case Failure(exception) =>
      println(s"Error: Cannot parse puzzle input : ${exception.getMessage}")
    case Success(scratchGame) =>
      println(s"The total number of point is : ${scratchGame.getGamePoints}")
      println(
        s"The total number of scratch cards at the end of the game is ${scratchGame.getTotalCardsAfterScratch}"
      )
