package day4

import day4.ScratchCard.calcPoint

import scala.util.Try

case class ScratchCard(
    id: Int,
    winningNumbers: List[Int],
    obtainedNumbers: List[Int],
    doubloons: Int = 0
):
  def getPoints: Int =
    calcPoint(nbMatchingNumbers)

  def nbMatchingNumbers: Int =
    winningNumbers.intersect(obtainedNumbers).length

  def nbCards: Int =
    doubloons + 1
end ScratchCard

object ScratchCard:
  private def calcPoint(goodNumbers: Int, previousPoint: Int = 0): Int =
    if goodNumbers == 0 then 0
    else if goodNumbers == 1 then 1
    else 2 * calcPoint(goodNumbers - 1)

  def createFromLine(line: String): Try[ScratchCard] =
    Try {
      def formatNumberListStr(numberList: String) =
        numberList
          .split(" ")
          .map(_.trim)
          .filter(_.nonEmpty)
          .map(_.toInt)
          .toList

      val inputRegex = """^Card.*(\d+): (.+) \| (.+)$""".r
      val inputRegex(cardId, winningNumbersStr, obtainedNumbersStr) =
        line

      ScratchCard(
        cardId.trim.toInt,
        formatNumberListStr(winningNumbersStr),
        formatNumberListStr(obtainedNumbersStr)
      )
    }

end ScratchCard
