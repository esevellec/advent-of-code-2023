package day3

import common.Helper

import scala.util.{Failure, Success, Try}

@main
def main(): Unit =
  val maybeEnginSchematic: Try[EngineSchematic] =
    Helper.parseFile("day3", "input.txt")
  maybeEnginSchematic match
    case Failure(exception) =>
      println(s"Error : Cannot parse puzzle input ${exception.getMessage}")
    case Success(engineSchematic) =>
      // println(engineSchematic)
      println(
        s"The sum of all of the part numbers in the engine schematic is ${engineSchematic.getNumbers.sum}"
      )
      println(
        s"The sum of all of the gear ratios in the engine schematic is ${engineSchematic.getGearRatio.sum}"
      )
