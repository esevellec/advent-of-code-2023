package day3

trait SchemaElement:
  val value: Char

object SchemaElement:
  def createElementFromChar(char: Char): SchemaElement =
    if char.isDigit then Digit(char)
    else Symbol(char)
end SchemaElement

case class Digit(value: Char)  extends SchemaElement
case class Symbol(value: Char) extends SchemaElement
