package day3

import scala.reflect.ClassTag
import scala.util.{Success, Try}

case class EngineSchematic(elements: List[List[Option[SchemaElement]]]):

  def getNumbers: List[Int] =
    getPotentialNumbers.map(_.value.toInt)

  def getGearRatio: List[Long] =
    val potentialNumbers = getPotentialNumbers
    val potentialNumbersPositions =
      potentialNumbers.map(_.adjacentPost).toSet
    val potentialGearsPositions =
      getElementPosition[Symbol]((s: Symbol) => s.value == '*')
    val validGearsPositions = potentialGearsPositions
      .filter(potentialGearPosition =>
        potentialNumbersPositions.count(numberPositions =>
          numberPositions.contains(potentialGearPosition)
        ) > 1
      )
    validGearsPositions.toList.map(vg =>
      potentialNumbers
        .filter(pn => pn.adjacentPost.contains(vg))
        .map(_.value.toInt)
        .product
    )

  private def getPotentialNumbers: List[PotentialNumber] =
    val digitElement  = filterSpecificElementType[Digit]
    val symbolElement = getElementPosition[Symbol]()
    val maybeNumbers = digitElement.zipWithIndex
      .flatMap((line, yIndex) =>
        getNumbersFromLine(formatLine(line), yIndex).getOrElse(List.empty)
      )
    maybeNumbers
      .filter(mn => mn.adjacentPost.intersect(symbolElement).nonEmpty)

  private def formatLine(line: List[Option[Digit]]) =
    line.map {
      case Some(digit) => digit.value
      case None        => " "
    }.mkString

  private def getElementPosition[T <: SchemaElement: ClassTag](
      filter: T => Boolean = (t: T) => true
  ): Set[Position] =
    filterSpecificElementType[T].zipWithIndex
      .flatMap((line, yIndex) =>
        line.zipWithIndex.flatMap((symbol, xIndex) =>
          symbol
            .filter(filter)
            .map(s => Position(xIndex, yIndex))
        )
      )
      .toSet

  private def filterSpecificElementType[T <: SchemaElement: ClassTag]
      : List[List[Option[T]]] =
    elements.map(line =>
      line.map(maybeElement =>
        maybeElement
          .collect { case t: T =>
            t
          }
      )
    )

  private def getNumbersFromLine(
      line: String,
      yIndex: Int
  ): Try[List[PotentialNumber]] =
    val digitsPosition = getElementPosition[Digit]()
    Try(
      line.zipWithIndex
        .foldLeft(List[Option[PotentialNumber]]())(
          (currentList, charWithIndex) =>
            val (char, xIndex) = charWithIndex

            def concatNewNumber =
              currentList :+ Some(
                PotentialNumber(
                  char.toString,
                  getSurroundingPosition(
                    Position(xIndex, yIndex),
                    line.length,
                    elements.length
                  )
                )
              )

            def concatCharToLastNumber =
              val last =
                currentList.last.getOrElse(
                  PotentialNumber("", Set.empty)
                )
              currentList.init :+ Some(
                last.copy(
                  value = last.value + char,
                  adjacentPost = last.adjacentPost ++ getSurroundingPosition(
                    Position(xIndex, yIndex),
                    line.length,
                    elements.length
                  )
                )
              )

            if char == ' ' then currentList :+ None
            else if currentList.isEmpty || currentList.last.isEmpty then
              concatNewNumber
            else concatCharToLastNumber
        )
        .flatten
        .map(pn => pn.copy(adjacentPost = pn.adjacentPost -- digitsPosition))
    )

  private def getSurroundingPosition(
      position: Position,
      maxX: Int,
      maxY: Int
  ): Set[Position] =
    (for
      xPos <- (position.x - 1 to position.x + 1)
      yPos <- (position.y - 1 to position.y + 1)
    yield Position(xPos, yPos)).toSet
      .filterNot(pos => pos.x < 0 || pos.x > maxX || pos.y < 0 || pos.y > maxY)

  override def toString: String =
    elements
      .map(line =>
        line
          .map {
            case Some(element) => element.value
            case None          => "."
          }
      )
      .map(_.mkString)
      .mkString("\n")
end EngineSchematic

object EngineSchematic:
  given (String => Try[EngineSchematic]) = createFromFile
  def createFromFile(fileContent: String): Try[EngineSchematic] =
    val lines = fileContent.split("\n")
    val elems = lines
      .map(line =>
        line
          .map(char =>
            if char.equals('.') then None
            else Some(SchemaElement.createElementFromChar(char))
          )
          .toList
      )
      .toList
    Success(EngineSchematic(elems))
end EngineSchematic
