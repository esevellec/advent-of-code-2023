package day3

case class PotentialNumber(
    value: String,
    adjacentPost: Set[Position]
)
