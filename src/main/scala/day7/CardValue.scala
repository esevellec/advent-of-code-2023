package day7

enum CardValue:
  case Joker, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen,
    King, As

object CardValue:

  def fromString(value: Char, withJoker: Boolean): Option[CardValue] =
    if value.toString.toIntOption.isDefined then
      val valInt = value.toString.toInt
      if valInt < 2 || valInt > 10 then None
      else Some(CardValue.fromOrdinal(value.toString.toInt - 1))
    else if value == 'T' then Some(CardValue.Ten)
    else if value == 'J' then
      if withJoker then Some(CardValue.Joker) else Some(CardValue.Jack)
    else if value == 'Q' then Some(CardValue.Queen)
    else if value == 'K' then Some(CardValue.King)
    else if value == 'A' then Some(CardValue.As)
    else None
end CardValue
