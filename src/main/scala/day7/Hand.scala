package day7

import day7.CardValue.Joker
import day7.Hand.compareHighCard
import day7.HandType.{FullHouse, ThreeOfAKind}

import scala.annotation.tailrec
import scala.collection.immutable

case class Hand(cards: String, bid: Int, withJoker: Boolean = false)
    extends Ordered[Hand]:
  private val cardsValue: List[CardValue] = cards
    .map(CardValue.fromString(_, withJoker))
    .collect { case Some(cardValue) =>
      cardValue
    }
    .toList

  private val handType: HandType =
    val nbJoker = cardsValue.count(_ == CardValue.Joker)

    val doubloons = cardsValue
      .filterNot(_ == CardValue.Joker)
      .groupBy(c => c.toString)
      .map((_, values) => values.length)
      .toList
      .sorted
      .reverse

    val doubloonsFirstCard =
      doubloons.headOption.map(_ + nbJoker).getOrElse(nbJoker)
    val doubloonsSecondCard =
      if doubloons.nonEmpty then doubloons.tail.headOption.getOrElse(0) else 0

    if doubloonsFirstCard == 5 then HandType.FiveOfAKind
    else if doubloonsFirstCard == 4 then HandType.FourOfAKind
    else if doubloonsFirstCard == 3 && doubloonsSecondCard == 2
    then HandType.FullHouse
    else if doubloonsFirstCard == 3 then HandType.ThreeOfAKind
    else if doubloonsFirstCard == 2 && doubloonsSecondCard == 2
    then HandType.TwoPair
    else if doubloonsFirstCard == 2 then HandType.OnePair
    else HandType.HighCard

  override def compare(that: Hand): Int =
    val handTypeCompare = handType.ordinal - that.handType.ordinal
    if handTypeCompare == 0 then compareHighCard(cardsValue, that.cardsValue)
    else handTypeCompare

end Hand

object Hand:
  @tailrec
  private def compareHighCard(
      firstHand: List[CardValue],
      secondHand: List[CardValue]
  ): Int =
    val compareHC = firstHand.head.ordinal - secondHand.head.ordinal
    if compareHC == 0 then compareHighCard(firstHand.tail, secondHand.tail)
    else compareHC
end Hand
