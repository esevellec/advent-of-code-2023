package day7

import common.Helper

import scala.util.{Failure, Try}

case class CamelCardsGame(hands: List[Hand]):
  def getTotalWinning: Int =
    hands.sorted.zipWithIndex.foldLeft(0)((currentWinning, t) =>
      val (hand, index) = t
      currentWinning + (index + 1) * hand.bid
    )
end CamelCardsGame

object CamelCardsGame:
  given (String => Try[CamelCardsGame]) = createFromFile(_)
  val withJoker: (String => Try[CamelCardsGame]) =
    CamelCardsGame.createFromFile(_, true)

  def createFromFile(
      fileContent: String,
      withJoker: Boolean = false
  ): Try[CamelCardsGame] =
    Helper
      .reverseTryList(
        fileContent
          .split("\n")
          .toList
          .map(line =>
            line.split(" ").toList match
              case hand :: bid :: Nil => Try(Hand(hand, bid.toInt, withJoker))
              case _ =>
                Failure(new Exception(s"Cannot parse Hand from line : $line"))
          )
      )
      .map(CamelCardsGame.apply)
end CamelCardsGame
