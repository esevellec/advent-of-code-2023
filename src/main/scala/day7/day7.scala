package day7

import common.Helper

import scala.util.{Failure, Success, Try}

@main
def main(): Unit =
  val maybeGame: Try[CamelCardsGame] = Helper.parseFile("day7", "input.txt")
  val maybeGameWithJokers: Try[CamelCardsGame] =
    Helper.parseFile("day7", "input.txt")(using CamelCardsGame.withJoker)
  (for {
    game <- maybeGame
    gameWithJoker <- maybeGameWithJokers
  } yield (game, gameWithJoker)) match
    case Failure(exception) =>
      println(s"Error : cannot parse puzzle input ${exception.getMessage}")
    case Success(game, gameWithJoker) =>
      println(s"The total winnings are ${game.getTotalWinning}")
      println(s"The total winnings with jokers are ${gameWithJoker.getTotalWinning}")
