package day6

import scala.annotation.tailrec

case class Race(time: Long, distance: Long):

  private def getFirstWin(timeButtonPressed: Long = 0): Long =
    getExtremum(timeButtonPressed, t => t + 1)

  private def getLastWin(timeButtonPressed: Long = time): Long =
    getExtremum(timeButtonPressed, t => t - 1)

  @tailrec
  private final def getExtremum(
      timeButtonPressed: Long,
      getNextVal: Long => Long
  ): Long =
    if isWon(timeButtonPressed) then timeButtonPressed
    else getExtremum(getNextVal(timeButtonPressed), getNextVal)

  def getTotalWinningPossibilities: Long =
    getLastWin() - getFirstWin() + 1
  private def isWon(buttonPressedTime: Long): Boolean =
    val remainingTime = time - buttonPressedTime
    BigInt(remainingTime) * BigInt(buttonPressedTime) > distance
end Race
