package day6

import common.Helper

import scala.util.{Failure, Try}

case class Competition(races: List[Race]):
  def getMultipliedNumberOfBrokenRecords: Long =
    getWinningPressTimes.product
  def getWinningPressTimes: List[Long] =
    races.map(_.getTotalWinningPossibilities)
end Competition

object Competition:
  given (String => Try[Competition]) = createFromFile

  def createSingleRaceFromFile(fileContent: String): Try[Competition] =
    val getListRegex = """\w+:\s*(.+)""".r

    getListRegex
      .findAllMatchIn(fileContent)
      .toList
      .flatMap(
        _.subgroups.headOption.toList
          .map(
            _.split(" ")
              .filter(_.nonEmpty)
              .mkString
          )
      ) match
      case time :: distance :: Nil =>
        Try(Race(time.toLong, distance.toLong)).map(r => Competition(List(r)))
      case _ =>
        Failure(
          new Exception(
            s"Cannot get times and distances from input : $fileContent"
          )
        )
  def createFromFile(fileContent: String): Try[Competition] =
    val getListRegex = """\w+:\s*(.+)""".r

    getListRegex
      .findAllMatchIn(fileContent)
      .toList
      .map(
        _.subgroups.headOption.toList
          .flatMap(
            _.split(" ")
              .filter(_.nonEmpty)
          )
      ) match
      case times :: distances :: Nil =>
        Helper
          .reverseTryList(
            times
              .zip(distances)
              .map((time, distance) =>
                Try(Race.apply(time.toLong, distance.toLong))
              )
          )
          .map(Competition.apply)
      case _ =>
        Failure(
          new Exception(
            s"Cannot get times and distances from input : $fileContent"
          )
        )
end Competition
