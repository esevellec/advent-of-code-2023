package day6

import common.Helper

import scala.util.{Failure, Success, Try}

@main
def day6(): Unit =
  val module     = "day6"
  val sourceFile = "input.txt"
  val maybeCompetition: Try[Competition] =
    Helper.parseFile(module, sourceFile)
  val maybeCompetitionUniqueRace: Try[Competition] =
    Helper.parseFile(module, sourceFile)(using
      Competition.createSingleRaceFromFile
    )

  (for
    competition           <- maybeCompetition
    competitionUniqueRace <- maybeCompetitionUniqueRace
  yield (competition, competitionUniqueRace)) match
    case Failure(exception) =>
      println(s"Error : Cannot parse puzzle input, ${exception.getMessage}")
    case Success(competition, competitionUniqueRace) =>
      println(
        s"When we multiply the number of ways to beat the record, we get ${competition.getMultipliedNumberOfBrokenRecords}"
      )
      println(
        s"You can beat the record in ${competitionUniqueRace.getMultipliedNumberOfBrokenRecords} ways in one much longer race"
      )
