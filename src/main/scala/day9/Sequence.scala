package day9

import common.Helper
import day9.Sequence.getSubSequences

import scala.annotation.tailrec
import scala.util.Try

case class Sequence(values: List[Int]):
  val subSequences: List[Sequence] = getSubSequences(List(this)).tail
  def getSubSequence: Sequence =
    val subValues =
      (1 until values.length)
        .map(index => values(index) - values(index - 1))
        .toList
    Sequence(subValues)

  def getNextValue: Int =
    subSequences.map(_.values.last).sum + values.last

  def getFirstValue: Int =
    values.head - subSequences
      .map(_.values.head)
      .foldRight(0)((current, value) => current - value)
end Sequence

object Sequence:

  given (String => Try[List[Sequence]]) = createFromFile
  @tailrec
  def getSubSequences(sequences: List[Sequence]): List[Sequence] =
    if sequences.isEmpty then sequences
    else
      val last = sequences.last
      if last.values.forall(_ == 0) then sequences
      else getSubSequences(sequences :+ last.getSubSequence)

  def createFromFile(fileContent: String): Try[List[Sequence]] =
    Helper.reverseTryList(
      fileContent
        .split("\n")
        .toList
        .map(line =>
          Helper
            .reverseTryList(
              line
                .split(" ")
                .toList
                .map(_.trim)
                .filter(_.nonEmpty)
                .map(value => Try(value.toInt))
            )
            .map(Sequence.apply)
        )
    )

end Sequence
