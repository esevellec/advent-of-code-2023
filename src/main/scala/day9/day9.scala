package day9

import common.Helper

import scala.util.{Failure, Success, Try}

@main
def main(): Unit =
  val maybeSequences: Try[List[Sequence]] =
    Helper.parseFile("day9", "input.txt")
  maybeSequences match
    case Failure(exception) =>
      println(s"Error : cannot parse puzzle input ${exception.getMessage}")
    case Success(sequences) =>
      println(
        s"The sum of all extrapolated last values is ${sequences.map(_.getNextValue).sum}"
      )

      println(
        s"The sum of all extrapolated first values is ${sequences.map(_.getFirstValue).sum}"
      )
