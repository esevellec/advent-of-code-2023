package day1

import common.Helper
import scala.util.Try

import scala.util.{Failure, Success}

@main
def main(): Unit =
  val maybeCalibrations: Try[List[Calibration]] =
    Helper.parseFile("day1", "input.txt")
  maybeCalibrations match
    case Failure(exception) =>
      println(s"Error when parsing puzzle input : ${exception.getMessage}")
    case Success(calibrations) =>
      val rawCalibrationSum =
        calibrations.map(_.getRawCalibrationValue).flatMap(_.toOption).sum
      val interpolatedCalibrationSum =
        calibrations
          .map(_.getInterpolatedCalibrationValue)
          .flatMap(_.toOption)
          .sum

      println(s"The raw calibrations sum is $rawCalibrationSum")
      println(
        s"The interpolated calibrations sum is $interpolatedCalibrationSum"
      )
