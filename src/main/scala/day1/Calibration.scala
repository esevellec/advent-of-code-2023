package day1

import scala.util.{Failure, Success, Try}

case class Calibration(line: String):
  import day1.Calibration.*

  def getRawCalibrationValue: Try[Int] =
    getCalibrationValue(line)

  def getInterpolatedCalibrationValue: Try[Int] =
    val splitString = splitInStringShards(line)
    if splitString.isEmpty then
      Failure(new Exception("No Digit found neither digits nor spelled"))
    else
      val head = splitString.head
      val tail = splitString.reverse.head
      val headReplaced = head.foldLeft("")((alreadyCheckedStr, newChar) =>
        val updated = alreadyCheckedStr + newChar
        replaceSpelledDigit(updated)
      )

      val tailReplaced = tail.foldRight("")((newChar, alreadyCheckedStr) =>
        val updated = newChar + alreadyCheckedStr
        replaceSpelledDigit(updated)
      )
      val mergeLineWithNewHeadTail =
        (headReplaced :+ splitString.tail.init.mkString :+ tailReplaced).mkString
      getCalibrationValue(mergeLineWithNewHeadTail)
  end getInterpolatedCalibrationValue

end Calibration

case object Calibration:
  given (String => Try[List[Calibration]]) = createFromFile
  def createFromFile(content: String): Try[List[Calibration]] =
    Success(content.split("\n").toList.map(Calibration.apply))

  private def getCalibrationValue(calibrationLine: String) =
    val digits = calibrationLine.filter(_.isDigit).toList
    Try(s"${digits.head}${digits.reverse.head}".toInt)

  private def splitInStringShards(fullString: String): List[String] =
    fullString.foldLeft(List[String]())((shards, nextChar) =>
      if shards.isEmpty then List(nextChar.toString)
      else if nextChar.isDigit || shards.last.toIntOption.isDefined then
        shards :+ nextChar.toString
      else shards.init :+ shards.last + nextChar
    )
  end splitInStringShards

  private def replaceSpelledDigit(string: String) =
    digitsSpelledToStr.foldLeft(string)((value, tuple) =>
      val (digitSpelled, digits) = tuple
      value.replace(digitSpelled, digits)
    )

  val digitsSpelledToStr: Map[String, String] = Map(
    "one"   -> "1",
    "two"   -> "2",
    "three" -> "3",
    "four"  -> "4",
    "five"  -> "5",
    "six"   -> "6",
    "seven" -> "7",
    "eight" -> "8",
    "nine"  -> "9"
  )
end Calibration
