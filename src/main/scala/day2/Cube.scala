package day2

import scala.util.{Failure, Success, Try}

sealed trait Cube:
  val nbCubes: Int
case class RedCube(nbCubes: Int)   extends Cube
case class GreenCube(nbCubes: Int) extends Cube
case class BlueCube(nbCubes: Int)  extends Cube

enum CubeColor:
  case Red, Green, Blue

case object Cube:
  given Conversion[String, Try[List[Cube]]] = createFromFile
  def createFromFile(cubesStr: String): Try[List[Cube]] =
    Try(
      cubesStr
        .split(",")
        .toList
        .map(_.trim)
        .map(cubeStr =>
          val regex                    = """^(\d+) (blue|red|green)$""".r
          val regex(cubeNumber, color) = cubeStr
          (color match
            case "blue"  => BlueCube(cubeNumber.toInt)
            case "red"   => RedCube(cubeNumber.toInt)
            case "green" => GreenCube(cubeNumber.toInt)
            case _ =>
              throw Exception(
                s"Cannot create correct cube from string $cubeStr"
              )
          )
        )
    )
