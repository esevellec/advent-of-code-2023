package day2

import common.Helper.reverseTryList

import scala.util.Try

case class Round(roundNumber: Int, pulls: List[Pull]):
  def isPossible(nbRedCube: Int, nbGreenCub: Int, nbBlueCube: Int): Boolean =
    isColorPossible(nbRedCube, CubeColor.Red) &&
      isColorPossible(nbGreenCub, CubeColor.Green) &&
      isColorPossible(nbBlueCube, CubeColor.Blue)

  def getGamePower(): Int =
    val minimalRedCubes: List[Int]   = getMinimalCubePerColor(CubeColor.Red)
    val minimalGreenCubes: List[Int] = getMinimalCubePerColor(CubeColor.Green)
    val minimalBlueCubes: List[Int]  = getMinimalCubePerColor(CubeColor.Blue)

    calculatePower(
      minimalRedCubes.max,
      minimalGreenCubes.max,
      minimalBlueCubes.max
    )

  private def getMinimalCubePerColor(cubeColor: CubeColor): List[Int] =
    pulls.map(_.getMinimalCubeNbPerColor(cubeColor))

  private def calculatePower(
      nbRedCube: Int,
      nbGreenCube: Int,
      nbBlueCube: Int
  ) = nbRedCube * nbGreenCube * nbBlueCube

  private def isColorPossible(nbCubes: Int, cubeColor: CubeColor): Boolean =
    !pulls
      .map(_.getCubeNbPerColor(cubeColor))
      .exists(nb => nb > nbCubes)

object Round:
  given (String => Try[List[Round]]) = readFromFile

  def readFromFile(fileContent: String): Try[List[Round]] =
    val lines = fileContent.split("\n").toList
    val regex = """^Game (\d+): (.*)$""".r

    Try {
      reverseTryList(
        lines
          .map(line =>
            val regex(gameNumber, maybePulls) = line
            val pulls: Try[List[Pull]]        = maybePulls
            pulls.map(pulls => Round(gameNumber.toInt, pulls))
          )
      )
    }.flatten
