package day2

import common.Helper

import scala.util.{Failure, Success, Try}

case class Pull(cubes: List[Cube]):
  def getCubeNbPerColor(cubeColor: CubeColor): Int =
    getCubeByColor(cubeColor)
      .map(_.nbCubes)
      .sum

  def getMinimalCubeNbPerColor(cubeColor: CubeColor): Int =
    val cubeByColor = getCubeByColor(cubeColor)
      .map(_.nbCubes)
    if cubeByColor.isEmpty then 0 else cubeByColor.min

  private def getCubeByColor(cubeColor: CubeColor) =
    cubes
      .collect {
        case c: RedCube if cubeColor == CubeColor.Red     => c
        case c: GreenCube if cubeColor == CubeColor.Green => c
        case c: BlueCube if cubeColor == CubeColor.Blue   => c
      }

end Pull

object Pull:
  given Conversion[String, Try[List[Pull]]] = createPullsFromString
  def createPullsFromString(pullsStr: String): Try[List[Pull]] =
    Try(
      pullsStr
        .split(";")
        .toList
        .map(_.trim)
        .map { maybeCubesStr =>
          val maybeCubes: Try[List[Cube]] = maybeCubesStr
          maybeCubes match
            case Failure(exception)         => throw exception
            case Success(cubes: List[Cube]) => Pull.apply(cubes)
        }
    )

end Pull
