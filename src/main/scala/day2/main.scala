package day2

import common.Helper

import scala.util.{Failure, Success, Try}

@main
def main(): Unit =
  val maybeGames: Try[List[Round]] =
    Helper.parseFile("day2", "input.txt")
  maybeGames match
    case Failure(exception) =>
      println(s"Error when parsing puzzle input : ${exception.getMessage}")
    case Success(games) =>
      println(
        s"The sum of the IDs of those games is ${sumPossibleGamesIds(games)}"
      )

      println(
        s"The sum of the power of these Sets is ${sumGamesPower(games)}"
      )

def sumPossibleGamesIds(games: List[Round]): Int =
  games.filter(_.isPossible(12, 13, 14)).map(_.roundNumber).sum

def sumGamesPower(games: List[Round]): Int =
  games.map(_.getGamePower()).sum
