package day5

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

trait Seed:
  val number: Long
  def isLast: Boolean
  def getNext: Seed

case class ClassicSeed(number: Long) extends Seed:
  override def isLast: Boolean = true
  override def getNext: Seed = throw new Exception(
    "Can't get Next Seed of a classic Seed"
  )
end ClassicSeed

case class RangeSeed(number: Long, maxNumber: Long) extends Seed:
  override def isLast: Boolean = number == maxNumber
  override def getNext: Seed   = this.copy(number = number + 1)
end RangeSeed

object Seed:
  def createSeedsFromLine(line: String): Try[List[Seed]] =
    Try(
      line
        .split(" ")
        .map(_.trim)
        .filter(_.nonEmpty)
        .map(_.toLong)
        .toList
        .map(ClassicSeed.apply)
    )
  def createSeedsFromRange(seedRange: String): Try[List[Seed]] =
    @tailrec
    def buildPairs(
        seedsValue: List[Long],
        currentPairs: List[(Long, Long)] = List.empty
    ): Try[List[(Long, Long)]] =
      seedsValue match
        case head :: Nil =>
          Failure(new Exception("Can't build pairs, impaired number of seeds"))
        case seedNumber :: range :: tail =>
          buildPairs(tail, currentPairs :+ (seedNumber, range))
        case Nil => Success(currentPairs)

    val trySeedsValue = Try {
      seedRange
        .split(" ")
        .map(_.trim)
        .filter(_.nonEmpty)
        .map(_.toLong)
        .toList
    }

    trySeedsValue.flatMap(seedsValue =>
      buildPairs(seedsValue).map(pairs =>
        pairs
          .map((seedValue, range) =>
            RangeSeed(seedValue, seedValue + range - 1)
          )
      )
    )
end Seed
