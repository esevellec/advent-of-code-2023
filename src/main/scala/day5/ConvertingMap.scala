package day5

import scala.util.Try
import common.Helper

case class ConvertingMap(
    convertingType: String,
    ranges: List[Range]
):
  def getDestination(source: Long): Long =
    ranges.flatMap(_.applyRange(source)).headOption.getOrElse(source)

end ConvertingMap

object ConvertingMap:
  def createMapsFromLines(
      lines: String,
      convertingType: String
  ): Try[ConvertingMap] =
    Helper
      .reverseTryList(
        lines
          .split("\n")
          .toList
          .map(Range.apply)
      )
      .map(ranges => ConvertingMap(convertingType, ranges))

end ConvertingMap
