package day5

import common.Helper
import day5.Almanac.createFromFile

import scala.util.{Failure, Success, Try}

@main
def main(): Unit =
  val module                     = "day5"
  val fileName                   = "input.txt"
  val maybeAlmanac: Try[Almanac] = Helper.parseFile(module, fileName)

  val maybeAlmanacWithRange: Try[Almanac] =
    Helper.parseFile(module, fileName)(using
      createFromFile(_, Seed.createSeedsFromRange)
    )

  (for
    almanac          <- maybeAlmanac
    almanacWithRange <- maybeAlmanacWithRange
  yield (almanac, almanacWithRange)) match
    case Failure(exception) =>
      println(s"Error : cannot parse puzzle input : ${exception.getMessage}")
    case Success((almanac, almanacWithRange)) =>
      println(s"The lowest location number is ${almanac.getLowestLocation}")
      println(
        s"The lowest location number is ${almanacWithRange.getLowestLocation}"
      )
