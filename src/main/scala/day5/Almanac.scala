package day5

import scala.util.{Failure, Success, Try}
import common.Helper
import day5.Almanac.getSeedMinimalLocation

import scala.annotation.tailrec

case class Almanac(
    seeds: List[Seed],
    convertingMaps: List[ConvertingMap]
):

  def getLowestLocation: Long =
    seeds.map(seed => getSeedMinimalLocation(seed, convertingMaps)).min

end Almanac

object Almanac:
  given (String => Try[List[Seed]]) = Seed.createSeedsFromLine
  given (String => Try[Almanac]) = createFromFile(_, Seed.createSeedsFromLine)

  @tailrec
  private def getSeedMinimalLocation(
      seed: Seed,
      maps: List[ConvertingMap],
      currentMin: Long = Long.MaxValue
  ): Long =
    if seed.isLast then
      math.min(currentMin, getSeedValueFinalLocation(seed.number, maps))
    else
      getSeedMinimalLocation(
        seed.getNext,
        maps,
        math.min(currentMin, getSeedValueFinalLocation(seed.number, maps))
      )

  private def getSeedValueFinalLocation(
      seedValue: Long,
      maps: List[ConvertingMap]
  ): Long =
    maps.foldLeft(seedValue)((location, currentMap) =>
      currentMap.getDestination(location)
    )

  def createFromFile(
      fileContent: String,
      seedsParser: String => Try[List[Seed]]
  ): Try[Almanac] =
    val seedRegex = """seeds: (.+)""".r
    val mapRegex  = """(\S+) map:\n((?>.+\n?)+)""".r

    val maybeSeedsStr = seedRegex
      .findFirstMatchIn(fileContent)
      .flatMap(_.subgroups.headOption)

    val mapsStr = mapRegex
      .findAllMatchIn(fileContent)
      .toList
      .flatMap(_.subgroups match
        case head :: tail :: Nil => Some(head, tail)
        case _                   => None
      )

    val tryMaps = Helper
      .reverseTryList(
        mapsStr.map((mapType, mapsStr) =>
          ConvertingMap.createMapsFromLines(mapsStr, mapType)
        )
      )

    for
      seedStr <- maybeSeedsStr
        .toRight(
          new Exception(s"Can't find seeds in pattern $fileContent")
        )
        .toTry
      seed <- seedsParser(seedStr)
      maps <- Helper
        .reverseTryList(
          mapsStr.map((mapType, mapsStr) =>
            ConvertingMap.createMapsFromLines(mapsStr, mapType)
          )
        )
    yield Almanac(seed, maps)
end Almanac
