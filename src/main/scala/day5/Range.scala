package day5

import scala.util.Try

case class Range(
    destinationRangeStart: Long,
    sourceRangeStart: Long,
    rangeLength: Long
):
  def applyRange(source: Long): Option[Long] =
    if sourceRangeStart <= source && source < sourceRangeStart + rangeLength
    then Some(destinationRangeStart + source - sourceRangeStart)
    else None
end Range

object Range:
  def apply(line: String): Try[Range] =
    Try {
      val extractedNumbers =
        line
          .split(" ")
          .map(_.trim)
          .filter(_.nonEmpty)
          .map(_.toLong)
          .toList

      extractedNumbers match
        case (sourceStart :: destStart :: length :: Nil) =>
          Range(sourceStart, destStart, length)
        case _ => throw new Exception(s"Cannot parse Range : $line")
    }
end Range
